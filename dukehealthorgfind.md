# Www.dukehealth.org/find-doctors-physicians Assessment

__<https://www.dukehealth.org/find-doctors-physicians>__

__Screenshot:__

![Screenshot of this website](assets/screenshot-find.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).




## Schedule Online

Nearly flawless! Amazing accessibility work.

__Visual location:__

![popup](assets/schedule-app.png)

__HTML location:__

```html
<a href="/schedule-doctor-physician?id=397193&amp;format=simple" data-scheduleonline="Matthew Abbott, MD" class="button schedule-online no-border" aria-label="Schedule Online for Doctor Matthew Abbott, MD">
  Schedule Online
  <i class="fas fa-external-link"></i>
</a>
```

### Add Opens in new window note.

I can't express how amazing the "Schedule Online" wizard experience is for PC Screen reader users. That was some fantastic and complicated accessibility work was. Major accomplishment and major kudos.

Only one thing to put it over the top...

#### Suggested solution:

Add a note that clicking the "Schedule Online" link it opens in a new window to screen reader users.  (It already has the visual indicator for sighted users. Nice work!)

Consider adding to the `aria-label`.  Something link `aria-label="Schedule Online for Doctor Matthew Abbott, MD. Opens in a new window"` or "Opens in a new windows" as screen reader only text. Whatever is easiest to add that notification.


---

<br>


## Elements with an ARIA `[role]` that require children to contain a specific `[role]` are missing some or all of those required children. [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships)

Some ARIA parent roles must contain specific child roles to perform their intended accessibility functions. [Learn more](https://web.dev/aria-required-children/).


## `[aria-*]` attributes do not have valid values [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing) [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value">WCAG 4.1.2)

Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid values. [Learn more](https://web.dev/aria-valid-attr-value/).

__Success, after main search globally fixed from homepage assessment.__

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Invalid ARIA attribute value: aria-controls=&#34;ex1-listbox&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,1,HEADER,0,NAV,0,DIV,2,DIV,3,DIV,0,FORM,0,DIV,0,DIV,1,INPUT`<br>
Selector:<br>
`#globalSearchInput`
</details>

---

<br>

### This element's Required ARIA child role not present: textbox 

__Visual location:__

![aria missing required children roles](assets/dukehealth-org-find-doctors-physiciansform-action-find-doctors-physicians-auto-input-container-react-autosuggest__container-role-combobox-aria-haspopup-listbox.png)


__HTML location:__

```html
<div role="combobox" aria-haspopup="listbox" aria-owns="react-autowhatever-docName_input" aria-expanded="false" class="react-autosuggest__container">
```

#### Suggested solution:

Do nothing. Probably a false positive. 

I think the target of the input exists after typing begins. So nothing to do here.


<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,0,DIV,0,SECTION,0,DIV,0,DIV,0,SECTION,3,DIV,0,FORM,1,DIV,1,DIV`<br>
Selector:<br>
`form[action="\/find-doctors-physicians"] > .auto-input-container > .react-autosuggest__container[role="combobox"][aria-haspopup="listbox"]`
</details>

---

<br>




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>



## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Success!__

---

<br>



## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Succcess!__

---

<br>




## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

Pay special attention to popup windows like ads or email signup solicitations. 

### Popup modal issue [Critical blocker]

Popup blocking assess to page for non-mouse users. [Critical blocker]

![popup](assets/popup2.png)

This popup must be removed or replaced.

- User cannot tab into it.
- User did not request the popup to show up.
- Popup does not inform screen readers that new content has been added to the page.
- Impossible for keyboard only users to dismiss the message, making the entire webpage useless.
- Focus is not managed. 

If the popup cannot be removed, the barrier will need mitigated. Focus will need managed along with a significant amount of ARIA attributes to make it accessible.

#### Suggested solution:

_Short-term:_

Priority here is mitigation first.  When this is visible, add event listener for the Escape key. When it is hit, dismiss the popup.  That will mitigate the problem, while a long term solution is implemented.

_Long-term:_

Follow the following design pattern:

<https://www.w3.org/TR/wai-aria-practices/examples/dialog-modal/alertdialog.html>

Replace with an accessible popup from a framework like Bootstrap 4.

---

<br>



## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---

<br>



## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value) [Critical blockers]

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).


### "Clinic name" input label mismatch

If the `<label>` exists it should match the `aria-label` to avoid confusing screen reader users.

__Visual location:__

![label wrong](assets/clinic-name-input.png)

__HTML location:__

```html
<label class="input-label visually-hidden" for="practiceLocation">practiceLocation</label>
<select class="native-dropdown selectbox-input no-selection" id="practiceLocation" aria-label="Clinic's Location">  
```

#### Suggested solution:

Change `<label>` text from "practiceLocation" to "Clinics Location".
 

---

<br>

### "Patient's Age" input label mismatch

If the `<label>` exists it should match the `aria-label` to avoid confusing screen reader users.

__Visual location:__

![label age mismatch](assets/patage.png)

__HTML location:__

```html
<label class="input-label visually-hidden" for="treatmentAge">treatmentAge</label>
<select class="native-dropdown selectbox-input no-selection" id="treatmentAge" aria-label="Patient's age">
```

#### Suggested solution:

Change `<label>` text from "treatmentAge" to "Patients age".

---

<br>


### "Provider's Gender" input label mismatch

If the `<label>` exists it should match the `aria-label` to avoid confusing screen reader users.

__Visual location:__

![label gender mismatch](assets/gender.png)

__HTML location:__

```html
<label class="input-label visually-hidden" for="gender">gender</label>
<select class="native-dropdown selectbox-input no-selection" id="gender" aria-label="Patient's age">
```

#### Suggested solution:

Change `aria-label` text from "Patient's age" to "Providers gender".

---

<br>

### "Languages" input label mismatch

If the `<label>` exists it should match the `aria-label` to avoid confusing screen reader users.

__Visual location:__

![label lang mismatch](assets/lang.png)

__HTML location:__

```html
<label class="input-label visually-hidden" for="language">language</label>
<select class="native-dropdown selectbox-input no-selection" id="language" aria-label="Patient's age">
```

#### Suggested solution:

Change `aria-label` text from "Patient's age" to "Languages".

---

### Missing label on faux combobox (false positive)

__Visual location:__

![aria missing required children roles](assets/dukehealth-org-find-doctors-physiciansform-action-find-doctors-physicians-auto-input-container-react-autosuggest__container-role-combobox-aria-haspopup-listbox.png)



__HTML location:__

![false pos](assets/false-positive.png)

#### Suggested solution/ explanation:

Automated tests indicate that there is a missing `<label>` or `aria-label`. Technically that is true.

Notice that the green elements reference each other in a normal label input relationship.

Notice the pink `<div role="combobox" ...>`. Adding that role makes it a real combobox form input to the browser's accessibility API.  Now that it is a real input, it wants it to be labeled.  It has no `id` so it cannot be referenced by a `<label>`, which makes the automated tests think it needs an `aria-label`.

However, all WCAG ARIA reference documents I have found implement the exact design pattern you have here. So its fine as-is, likely because the `aria-owns`, but since there is no clear guidance from official sources, lets leave it alone.

Do nothing.


---

<br>

## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).


### Zip code has wrong ARIA role

From Screen reader testers:

__Step 3: In the filter area, enter “27707” in the “City or Zip Code” filter.__

Screen reader user Jennifer:

>_"I couldn't find an edit box in the city or zip code filter. I pressed the city or zip code button, but there was no edit box. I put the zip code in the search box at the top of the page, and tried pressing the zip code button, but nothing happened."_

Screen reader user Niccolina:

>_"I couldn't find an edit box in the city or zip code filter. I pressed the city or zip code button, but there was no edit box. I put the zip code in the search box at the top of the page, and tried pressing the zip code button, but nothing happened."_

The "Use My Current Location" link can not be activated by screen reader users

```html
<input role="button" aria-label="City or Zip Code" type="search" id="locatedNear" placeholder="City or Zip Code" value="">
```

#### Suggested solution:

Remove `role="button"` from input.



---

<br>

## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

__Success!__

---

<br>

## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>

## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__

---

<br>

---

<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.