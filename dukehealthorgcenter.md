# Www.dukehealth.org/treatments/cancer Assessment Treatment Centers 

__<https://www.dukehealth.org/treatments/cancer>__

__Screenshot:__

![Screenshot of this website](assets/screenshot-cancer.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).


## `[aria-*]` attributes do not have valid values [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing) [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value">WCAG 4.1.2)

Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid values. [Learn more](https://web.dev/aria-valid-attr-value/).



__Success, after main search globally fixed from homepage assessment.__



<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Invalid ARIA attribute value: aria-controls=&#34;ex1-listbox&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,1,HEADER,0,NAV,0,DIV,2,DIV,3,DIV,0,FORM,0,DIV,0,DIV,1,INPUT`<br>
Selector:<br>
`#globalSearchInput`
</details>

---



## Image elements do not have `[alt]` attributes [WCAG 1.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#non-text-content)

Informative elements should aim for short, descriptive alternate text. Decorative elements can be ignored with an empty alt attribute. [Learn more](https://web.dev/image-alt/).



__Visual location:__

Image missing `alt` attribute:

![Image missing alt tag](assets/dukehealth-org-treatments-cancer-headerImag.png)


__HTML location:__

```html
<img src="https://www.dukehealth.org/sites/default/files/treatment_group/treatment_infusion_1500x4302.jpg" id="headerImage">
```

#### Suggested solution:

Add an `alt` attribute with an accurate description to the image or add invisible screen reader text.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Element does not have an alt attribute
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element has no title attribute or the title attribute is empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,0,DIV,0,FIGURE,0,IMG`<br>
Selector:<br>
`#headerImage`
</details>

---



## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).





### A link name of _"Home"_ might not a descriptive name to a screen reader. [Priority]

A screen reader user might just hear _"Link Home "_. If that clearly indicates its purpose and where it will take the them, it is ok.  If it is vauge like, "Read more" or "Learn more", it won't make sense out of context and it must be changed.

__Visual location:__

![Home not descriptive](assets/dukehealth-org-treatments-cancer-home-link-a-href.png)

#### HTML location:

```html
<a href="/">
  <span class="hiddenAcc">Home</span>
  <i class="icon icon-home"></i>
</a>
```

#### Suggested solution:

__Needs work.__


NOTE: This is a repeat from the Doctor landing page. If that change propagated, this is already fixed.

Using `display:none` hides text entirely from screen reader users. That makes this an empty link.

We don't need to hide any elements in the rating system, because the stars are background images, which are already ignored by screen readers.  We do need the text that is hidden though to provide context.

Root problem is that 'class=hiddenAcc" is hiding it using `display:none`. That is not an accessible method to do screen reader accessible text. It removes it entirely.

```css
.hiddenAcc {
    display: none;
}
```

#### Suggested solution:

Change `.hiddenAcc` class's properties to:

```css
.hiddenAcc {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0;
}
```

Visually there will be no difference.

`.hiddenAcc` class is likely used in other locations of the website. This probably fixes more than just that one element.





<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,0,DIV,2,SECTION,0,DIV,0,DIV,0,DIV,0,DIV,0,SPAN,0,A`<br>
Selector:<br>
`.home-link > a[href="\/"]`
</details>

<br>

---




## `[id]` attributes on the page are not unique [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing)

The value of an id attribute must be unique to prevent other instances from being overlooked by assistive technologies. [Learn more](https://web.dev/duplicate-id/).



<h3> 
  Document has multiple static elements with the same id attribute </h3>

__Visual location:__

![ element with duplicate ID](assets/dukehealth-org-treatments-cancer-fad_ct.png)

__HTML location:__

```html
<section id="2_fad_cta" class="content-block fad_cta ">
```

#### Suggested solution:

1. Check if the page needs that ID for CSS for visual reasons.
2. Check if the page needs that ID for JS for interactive behaviors.
3. If it needs the IDs edit the dependent code, then remove duplicate IDs from the HTML.

<details>
<summary>_Other options:_</summary>
Fix any of the following:
<br>Document has multiple static elements with the same id attribute
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,0,DIV,2,SECTION,0,DIV,1,DIV,2,SECTION`<br>
Selector:<br>
`.fad_cta`
</details>

---





## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>






## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Success!__

---

<br>

## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__

---

<br>


## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

Pay special attention to popup windows like ads or email signup solicitations. 

### Popup modal issue 

Success after "Popup blocking assess to page for non-mouse users." [Critical blocker] is fixed from the homepage.

### Rotating banner

The rotating image gallery does not announce when new images are added to screen reader software. It would need `aria-live` type solution.  Due to the contents, and the value to the audience who experiences the barrier, this is a low priority.  Plus, its location at the very bottom of the page makes it less likely to be used. This would be the lowest priority issue to work on when you have no other work to do in life, because of the factors mentioned above. Some rotating banners have critical information and need to be perfect, but this one is less important.

---

<br>

## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---

<br>



## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success!__

---


<br>



## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success!__

---


<br>



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

[Textise](https://www.textise.net/) is a neat tool for inspecting the natural order of the website. [View this website on Textise](https://www.textise.net/showText.aspx?strURL=www.dukehealth.org/treatments/cancer). If nothing has been done in JS to interfere the natural tab order, looking at that or viewing the source will basically follow the order of the markup.

__Success!__

---


<br>




## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>



## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__

---


<br>


<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.