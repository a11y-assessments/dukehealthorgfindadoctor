# Www.dukehealth.org/locations/duke-eye-center Location Assessment

__<https://www.dukehealth.org/locations/duke-eye-center>__

__Screenshot:__

![Screenshot of this website](assets/screenshot-eye.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).


## Schedule Online

Flawless. Awesome work.

![popup](assets/schedule-app.png)

__Success!__

---

<br>

## `[aria-*]` attributes do not have valid values [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing) [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value">WCAG 4.1.2)

Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid values. [Learn more](https://web.dev/aria-valid-attr-value/).

__Success, after main search globally fixed from homepage assessment.__

<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Invalid ARIA attribute value: aria-controls=&#34;ex1-listbox&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,1,HEADER,0,NAV,0,DIV,2,DIV,3,DIV,0,FORM,0,DIV,0,DIV,1,INPUT`<br>
Selector:<br>
`#globalSearchInput`
</details>

---

<br>


## Buttons do not have an accessible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

When a button doesn't have an accessible name, screen readers announce it as "button", making it unusable for users who rely on screen readers. [Learn more](https://web.dev/button-name/).



### This `button` or `input` has no `value` attribute or it contains no inner text to indicate its purpose

__Visual location:__

![button is not descriptive](assets/loc-eye-slide.png)

#### HTML location:

```html
<a class="carousel-control-prev border-bottom-0" href="#mediaCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true">
      <span class="sr-only">Previous</span>
    </span>
</a>
```

#### Suggested solution:

Link arrow button has no accessible text.

The Word "Previous exists, but is hidden from assistive tech because of the `aria-hidden` attribute.

To fix, remove `aria-hidden="true"`.  




<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,1,DIV,0,DIV,2,SECTION,1,SECTION,11,SECTION,1,DIV,4,DIV,0,DIV,6,A`<br>
Selector:<br>
`.carousel-control-prev[hrefS="\#mediaCarousel"][data-slide="prev"]`
</details>

---


## Links do not have a discernible name [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

Link text (and alternate text for images, when used as links) that is discernible, unique, and focusable improves the navigation experience for screen reader users. [Learn more](https://web.dev/link-name/).





### A link name of _"Rating details"_ might not a descriptive name to a screen reader.

A screen reader user might just hear _"Link Rating details "_. If that clearly indicates its purpose and where it will take the them, it is ok.  If it is vauge like, "Read more" or "Learn more", it won't make sense out of context and it must be changed.

__Visual location:__

![Rating details not descriptive](assets/dukehealth-org-locations-duke-eye-center-doc-2-ind-doctor-text-align-center-equal-height-row-space-nth-child-4-star-ratings-container-.png)

__HTML location:__

```html
<a href="/find-doctors-physicians/michael-allingham-md-phd?ratings=true">
  <span class="hiddenAcc">Rating details</span>
  <div class="filled-stars" style="width: 83.64px">
    <span></span><span></span><span></span><span></span><span></span>
  </div>
</a>
```

#### Suggested solution:

Fix `.hiddenAcc` class as mentioned in Duke Health Doctor page assessment.  If it was already fixed and propagated, then this task is complete.



<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,1,DIV,0,DIV,2,SECTION,1,SECTION,8,SECTION,3,DIV,1,DIV,1,DIV,0,DIV,7,DIV,0,DIV,1,A`<br>
Selector:<br>
`.doc-2.ind-doctor.text-align-center > .equal-height > .row-space:nth-child(4) > .star-ratings-container > a`
</details>

<br>

---

<br>



### The next arrow `<a>` link has no text inside.

__Visual location:__

![ not descriptive](assets/dukehealth-org-locations-duke-eye-center-scroll-carousel-nav-nex.png)

__HTML Location__:

```html
<a class="scroll carousel-nav next" href="javascript:void(0)">
  <i class="fa fa-chevron-right"></i>
</a>
```

Empty links are not read to a screen reader user, as a result, they will have no idea what the link does or where it would take them.

#### Suggested solution:

This arrow link does not do anything that screen reader users can't do based on the big next link overlay.

Consider adding `aria-hidden="true"` in this case, since it is duplicitive.



<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Element is in tab order and does not have accessible text

Fix any of the following:
<br>Element does not have text that is visible to screen readers
<br>aria-label attribute does not exist or is empty
<br>aria-labelledby attribute does not exist, references elements that do not exist or references elements that are empty
<br>Element&#39;s default semantics were not overridden with role=&#34;presentation&#34;
<br>Element&#39;s default semantics were not overridden with role=&#34;none&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,5,MAIN,0,DIV,1,DIV,1,DIV,0,ARTICLE,1,DIV,0,DIV,2,SECTION,1,SECTION,11,SECTION,1,DIV,5,DIV,2,DIV,0,A`<br>
Selector:<br>
`.scroll.carousel-nav.next`
</details>

<br>

---



---


<br>

## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>

## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

### Missing keyboard `:focus` on next arrow

__Visual location:__

![button is not descriptive](assets/loc-eye-slide.png)

#### HTML location:

```html
<a class="carousel-control-prev border-bottom-0" href="#mediaCarousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true">
      <span class="sr-only">Previous</span>
    </span>
</a>
```

__Needs work.__

Related CSS issue, the `:focus` indicator is hidden on the next and previous links.  remove `outline:0` to restore the browser default styles which are compliant.

```css
.carousel-control-prev:hover,.carousel-control-prev:focus,
.carousel-control-next:hover,.carousel-control-next:focus {
    color: #fff;
    text-decoration: none;
    /* outline: 0; */
    opacity: 0.9;
}
```

---


## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

### Overlay next link button are hidden 

Next/Previous buttons only show up on hover.

Touch screens and screen reader users have no hover state. 

#### Suggested solution:

To fix, remove `display:none` here and increase the contrast ratio via increasing `opacity` from `0.6` to `1`:

```css
.carousel#mediaCarousel a.carousel-control-next .carousel-control-prev-icon,
.carousel#mediaCarousel a.carousel-control-next .carousel-control-next-icon,
.carousel#mediaCarousel a.carousel-control-prev .carousel-control-prev-icon,
.carousel#mediaCarousel a.carousel-control-prev .carousel-control-next-icon {
    /* display: none; */
    width: 48px;
    height: 63px;
    background-size: 50%;
}
.carousel#mediaCarousel .carousel-inner:hover span.carousel-control-prev-icon,
.carousel#mediaCarousel .carousel-inner:hover span.carousel-control-next-icon {
    background-color: #000;
    /* opacity: 0.6; */
    opacity: 1;
    display: flex;
}
```

Or accomplish the same result in an additive way to override it.

---

<br>

## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

Pay special attention to popup windows like ads or email signup solicitations. 

### Popup modal issue 

Success after "Popup blocking assess to page for non-mouse users." [Critical blocker] is fixed from the homepage.

---

<br>



## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).


__Success!__

---

<br>

## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success!__

---

<br>



## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success!__

---

<br>

## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).


__Success!__

---

<br>


## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>



## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---

<br>



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__

---

<br>


<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.