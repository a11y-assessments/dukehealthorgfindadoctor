# Www.dukehealth.org/ Homepage Assessment and global issues

__<https://www.dukehealth.org/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot-home.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).





## `[aria-*]` attributes do not have valid values [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 4.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#parsing) [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value">WCAG 4.1.2)

Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid values. [Learn more](https://web.dev/aria-valid-attr-value/).



### This element is using invalid aria attributes. [Priority]

__Visual location:__

![aria valid attribute value problem](assets/dukehealth-org-globalSearchInpu.png)


__HTML location:__

```html
<input type="text" autocomplete="off" class="form-control" id="globalSearchInput" placeholder="Search" name="s" onkeyup="window.globalSearch.searchInputKeyUp(event)" aria-autocomplete="list" aria-controls="ex1-listbox" aria-activedescendant="">
```

#### Suggested solution:

Search input references `aria-controls="ex1-listbox" which does not exist.  

Also remove `aria-autocomplete="list"` and `aria-activedescendant"` they are not doing anything.

Remove `aria-controls` attribute.

More info:

<https://www.digitala11y.com/aria-activedescendant-properties/>



<details>
<summary>_Other options:_</summary>
Fix all of the following:
<br>Invalid ARIA attribute value: aria-controls=&#34;ex1-listbox&#34;
</details>

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,1,HEADER,0,NAV,0,DIV,2,DIV,3,DIV,0,FORM,0,DIV,0,DIV,1,INPUT`<br>
Selector:<br>
`#globalSearchInput`
</details>

---




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://web.dev/logical-tab-order/).

__Success!__

---

<br>

## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://web.dev/focusable-controls/).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Success!__

---

<br>





## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://web.dev/interactive-element-affordance/).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__

---

<br>





## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input) [Priority]

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://web.dev/managed-focus/).

Pay special attention to popup windows like ads or email signup solicitations. 

### Main menu dropdown needs ARIA attributes

__Visual location:__

![Main menu drop](assets/mainmenudrop.png)

__HTML location:__

```html
<div id="megaMenu" class="collapse navbar-collapse megamenu-collapse">
  <ul class="navbar-nav mr-auto" id="menuFirstLayer">
    <li class="nav-item d-lg-none">
      <a class="nav-link home-nav-link" href="/">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link fad-nav-link" href="/find-doctors-physicians">Find a Doctor</a>
    </li>
    <li class="nav-item dropdown">
      <a href="javascript:void(0)" class="nav-link dropdown-toggle treatments-nav-link" data-toggle="popover" data-dropdown="treatmentsDropdown">Treatments</a>
        <div id="treatmentsDropdown" class="dropdown-menu" style="display: none;">
        ...
      </div>
    </li>
    <li class="nav-item"></li>
    ...
    <li class="nav-item"></li>
  </ul>
</div>
```

This menu works perfectly for sighted keyboard-only users. But not for screen reader users. It won't take much to remediate it.

The issue is that a screen reader user, would not know there is a drop down menu because it is not announced.

Back in the old [assessment](https://docs.google.com/document/d/1UKM9-CKXbrtmRkgIZtMuCJ2OrLmtIe8QVZ2r9lMo6lo/edit?usp=sharing) from a few years ago, we mentioned replacing it with a more accessible menu. It seems about the same as it was. But we were not specific on what the problems were.  

>_"Using a menu system that uses aria attributes to communicate to the user if a menu item is active.  Aria attributes can also alert a person if the menu item has children in a drop-down menu. They also indicate if the drop-down is open or closed."_


#### Suggested solution:

- Add `aria-haspopup="true"` to the top level link that have a dropdown.
- Add `aria-expanded="true/false"` to the top level link.  (True or False needs toggled to describe the state of it's dropdown menu below)

Thats it. Using those two ARIA attributes on the triggering item is the only ARIA features necessary because the relationship to the dropdown is implied semantically in the DOM.

Example code for example item with dropdown after ARIA attributes added:

```html
<li class="nav-item dropdown">
~  <a aria-haspopup="true" aria-expanded="false" href="javascript:void(0)" class="nav-link ..." data-toggle="popover" data-dropdown="treatmentsDropdown">Treatments</a>
    <div id="treatmentsDropdown" class="dropdown-menu" style="display: none;">
      ...
  </div>
</li>
```

(And toggle `aria-expanded` to `true` when dropdown content is visible.)


---

### Popup blocking access to page for non-mouse users. [Critical blocker]

![popup](assets/pop.png)

This popup must be removed or replaced.

- User cannot tab into it.
- User did not request the popup to show up.
- Popup does not inform screen readers that new content has been added to the page.
- Impossible for keyboard only users to dismiss the message, making the entire webpage useless.
- Focus is not managed. 

If the popup cannot be removed, the barrier will need mitigated. Focus will need managed along with a significant amount of ARIA attributes to make it accessible.

#### Suggested solution:

_Short-term:_

Priority here is mitigation first.  When this is visible, add event listener for the Escape key. When it is hit, dismiss the popup.  That will mitigate the problem, while a long term solution is implemented.

_Long-term:_

Follow the following design pattern:

<https://www.w3.org/TR/wai-aria-practices/examples/dialog-modal/alertdialog.html>

Replace with an accessible popup from a framework like Bootstrap 4.


---

## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://web.dev/focus-traps/).

__Success!__

---





## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://web.dev/custom-controls-labels/).

__Success!__

---





## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://web.dev/custom-control-roles/).

__Success after "Main menu dropdown needs ARIA attributes" issue remediated.__

---

<br>



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://web.dev/visual-order-follows-dom/).

[Textise](https://www.textise.net/) is a neat tool for inspecting the natural order of the website. [View this website on Textise](https://www.textise.net/showText.aspx?strURL=www.dukehealth.org/). If nothing has been done in JS to interfere the natural tab order, looking at that or viewing the source will basically follow the order of the markup.

__Success!__

---

<br>

## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://web.dev/offscreen-content-hidden/).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---

<br>



## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://web.dev/heading-levels/).

__Success!__

---

<br>

## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://web.dev/use-landmarks/).

__Success!__

---


<br>

<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.